/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once



#define MESSAGE_HELPER0(x) #x
#define MESSAGE_HELPER1(x) MESSAGE_HELPER0(message x)
#define TODO_MESSAGE(x) _Pragma( MESSAGE_HELPER1(x) )
