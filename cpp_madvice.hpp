/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>
#include "cpp_target_os.hpp"
#include "cpp_debug.hpp"


#if defined(TARGET_LINUX) || defined(TARGET_UNIX)
    #include <sys/mman.h>
#else
    #include <limits>
#endif


namespace cppt
{


enum class EMADV
{
#if defined(TARGET_LINUX)
    normal     = MADV_NORMAL,
    random     = MADV_RANDOM,
    sequential = MADV_SEQUENTIAL,
    
    willneed   = MADV_WILLNEED,
    donotneed  = MADV_DONTNEED
#elif defined(TARGET_UNIX)
    normal     = POSIX_MADV_NORMAL,
    random     = POSIX_MADV_RANDOM,
    sequential = POSIX_MADV_SEQUENTIAL,
    
    willneed   = POSIX_MADV_WILLNEED,
    donotneed  = POSIX_MADV_DONTNEED
#else
    normal     = -std::numeric_limits<int>::max(),
    random,
    sequential,
    willneed,
    donotneed
#endif
}; // namespace madv


template <typename PT>
inline void memory_advise( PT* f_addr_p, size_t f_numBytes, EMADV f_flag_e )
{
#if defined(TARGET_LINUX) || defined(TARGET_UNIX)

    void *l_addr_p = reinterpret_cast<void*>( const_cast<typename std::remove_const<PT>::type*>(f_addr_p) );

    if ( (l_addr_p != NULL) && (f_numBytes > 0))
    {
        int l_val = madvise( l_addr_p, f_numBytes, static_cast<int>( f_flag_e ) );
        cpp_assert( l_val == 0, "Error in madvise" );
    }
    
#endif
}


template <typename PT, typename ...Args>
inline void memory_advise( PT *f_addr_p, size_t f_numBytes, EMADV f_flag_e, Args... more )
{
    memory_advise( f_addr_p, f_numBytes, f_flag_e );
    memory_advise( f_addr_p, f_numBytes, more... );
}


} // namespace cppt
