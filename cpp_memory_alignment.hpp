/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>
#include "cpp_type_traits.hpp"


namespace cppt
{

// ---------------------------------------------------------------------------------------


struct aligner_base
{
protected:

    uintptr_t m_preSize_u;
    uintptr_t m_alignedSize_u;
    uintptr_t m_postSize_u;

    inline aligner_base( uintptr_t f_arraySize_u );

public:
    inline bool has_pre_memory()     const { return (m_preSize_u     > 0); }
    inline bool has_aligned_memory() const { return (m_alignedSize_u > 0); }
    inline bool has_post_memory()    const { return (m_postSize_u    > 0); }

    inline void set_to_unalignable();

    inline uintptr_t num_aligned() const { return m_alignedSize_u; }
    inline uintptr_t num_unaligned() const { return (m_preSize_u+m_postSize_u); }
};

// ---------------------------------------------------------------------------------------


template <uintptr_t ITEM_SIZE, uintptr_t ALIGNMENT_SIZE>
struct array_aligner : public aligner_base
{
    static_assert( ITEM_SIZE <= ALIGNMENT_SIZE );
    static_assert( 0 == (ALIGNMENT_SIZE % ITEM_SIZE) );
    static_assert( 0 == (ALIGNMENT_SIZE % 2) );
    

    template <typename T>
    inline array_aligner( const T* f_array_p, uintptr_t f_arraySize_u );

    template <typename T>
    inline bool is_alignable_with( const T* f_array_p ) const;


    //template <typename T, typename = typename std::enable_if<cppt::is_iterator<T>::value>::type >
    template <typename T>
    inline T& pre_begin( T& f_array_p ) const { return f_array_p; }

    //template <typename T, typename = typename std::enable_if<cppt::is_iterator<T>::value>::type >
    template <typename T>
    inline T pre_end( T f_array_p ) const;

    //template <typename T, typename = typename std::enable_if<cppt::is_iterator<T>::value>::type >
    template <typename T>
    inline T aligned_begin( T f_array_p ) const { return pre_end( f_array_p ); }
    
    //template <typename T, typename = typename std::enable_if<cppt::is_iterator<T>::value>::type >
    template <typename T>
    inline T aligned_end( T f_array_p ) const;

    //template <typename T, typename = typename std::enable_if<cppt::is_iterator<T>::value>::type >
    template <typename T>
    inline T post_begin( T f_array_p ) const { return aligned_end( f_array_p ); }
    
    //template <typename T, typename = typename std::enable_if<cppt::is_iterator<T>::value>::type >
    template <typename T>
    inline T post_end( T f_array_p ) const;
    
private:

    inline bool align_with_start( const uintptr_t f_arrayStart_u );

    inline void align_with_end();
};

// ---------------------------------------------------------------------------------------


} // namespace cppt
