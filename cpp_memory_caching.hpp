/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

//#include <cstddef>
#include <cuchar>

// -----------------------------------------------------------------------

// see https://jdebp.uk/FGA/predefined-macros-processor.html

namespace cppt
{

/**
 * CACHE LINE SIZE
 */

#ifdef LEVEL1_DCACHE_LINESIZE
    constexpr std::size_t CACHE_LINE_SIZE = LEVEL1_DCACHE_LINESIZE;
#elif defined(ARM_ARCH) or defined(X86_32_ARCH)
    constexpr std::size_t CACHE_LINE_SIZE = 32u;
#elif defined(X86_64_ARCH)
    constexpr std::size_t CACHE_LINE_SIZE = 64u;
#endif

constexpr std::size_t CACHE_BLOCK_MASK = ~(CACHE_LINE_SIZE - 1u);

template <typename T>
constexpr std::size_t ITEMS_PER_CACHE_LINE() { return (CACHE_LINE_SIZE / sizeof(T)); }

/**
 * CACHE SIZE
 */
#ifdef LEVEL1_DCACHE_SIZE
    constexpr std::size_t L1_CACHE_SIZE = LEVEL1_DCACHE_SIZE;
#else
    constexpr std::size_t L1_CACHE_SIZE = 32768;
#endif



} // namespace cppt

