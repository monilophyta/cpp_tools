/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "cpp_madvice.hpp"
#include "cpp_iterator_bundling.hpp"


namespace cppt
{


template <typename ITER_T, typename ...Args>
inline 
void memory_advise( const CIteratorBundleIterator<ITER_T>& f_iter, size_t f_numItems, Args... madv_flags )
{
    const size_t f_numBytes = f_numItems * sizeof(ITER_T);

    const auto it_end = f_iter->end();
    for (auto it = f_iter->begin(); it != it_end; ++it )
    {
        memory_advise( &(*it), f_numBytes, madv_flags... );
    }
}



} // namespace cppt

