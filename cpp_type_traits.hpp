/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once

#include <type_traits>
#include <iterator>


namespace cppt
{

// ------------------------------------------------------------------------


template <typename T>
struct is_iterator 
    : public std::conditional<
        std::is_same< typename std::iterator_traits<T>::value_type, void>::value,
        std::false_type,
        std::true_type >::type
{};

// static_assert( false == is_iterator<int>::value );  - produces a compilation error!
static_assert( true == is_iterator<int*>::value );


// ------------------------------------------------------------------------

// available in C++20
template< class T >
struct remove_cvref
    : public std::remove_cv< typename std::remove_reference<T>::type >
{};



} // namespace cppt
