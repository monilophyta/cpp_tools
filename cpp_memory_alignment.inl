/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <iterator>
#include "cpp_memory_alignment.hpp"
#include "cpp_debug.hpp"


namespace cppt
{


// ---------------------------------------------------------------------------------------

inline aligner_base::aligner_base( uintptr_t f_arraySize_u )
    : m_preSize_u(0)
    , m_alignedSize_u( f_arraySize_u )
    , m_postSize_u( 0 )
{}


inline void aligner_base::set_to_unalignable()
{
    m_preSize_u += m_alignedSize_u;
    m_preSize_u += m_postSize_u;
    
    m_alignedSize_u = 0;
    m_postSize_u = 0;
}


// ---------------------------------------------------------------------------------------

template <uintptr_t ITEM_SIZE, uintptr_t ALIGNMENT_SIZE>
template <typename T>
inline array_aligner<ITEM_SIZE,ALIGNMENT_SIZE>::array_aligner( const T* f_array_p, uintptr_t f_arraySize_u )
    : aligner_base( f_arraySize_u )
{
    static_assert( ITEM_SIZE == sizeof(T) );

    if ( ALIGNMENT_SIZE > f_arraySize_u )
    {
        // Not enough space for alignment
        set_to_unalignable();
    }
    else
    {
        if ( true == align_with_start( reinterpret_cast<uintptr_t>(f_array_p) ) )
        {
            align_with_end();
        }
        else
        {
            cpp_assert( false == has_aligned_memory(), "Array should not be aligned" );
        }
    }
}


template <uintptr_t ITEM_SIZE, uintptr_t ALIGNMENT_SIZE>
template <typename T>
inline bool array_aligner<ITEM_SIZE,ALIGNMENT_SIZE>::is_alignable_with( const T* f_array_p ) const
{
    static_assert( ITEM_SIZE == sizeof(T) );
    
    if (false == has_aligned_memory())
        return false;
    
    const uintptr_t l_arrayStart_u = reinterpret_cast<uintptr_t>( f_array_p );
    
    // required preSize for f_array_p
    const uintptr_t l_preSize_u = l_arrayStart_u % ALIGNMENT_SIZE;

    // check if frontal overlap is the same
    if (l_preSize_u != (m_preSize_u * ITEM_SIZE))
        return false;
    
    return true;
}


template <uintptr_t ITEM_SIZE, uintptr_t ALIGNMENT_SIZE>
template <typename T>
inline T array_aligner<ITEM_SIZE,ALIGNMENT_SIZE>::pre_end( T f_array_p ) const
{ 
    static_assert( cppt::is_iterator<T>::value );
    //static_assert( sizeof(T) == ITEM_SIZE );
    return std::next(f_array_p, m_preSize_u); 
}


template <uintptr_t ITEM_SIZE, uintptr_t ALIGNMENT_SIZE>
template <typename T>
inline T array_aligner<ITEM_SIZE,ALIGNMENT_SIZE>::aligned_end( T f_array_p ) const
{ 
    static_assert( cppt::is_iterator<T>::value );
    //static_assert( sizeof(T) == ITEM_SIZE );
    return std::next(f_array_p, m_preSize_u + m_alignedSize_u); 
}


template <uintptr_t ITEM_SIZE, uintptr_t ALIGNMENT_SIZE>
template <typename T>
inline T array_aligner<ITEM_SIZE,ALIGNMENT_SIZE>::post_end( T f_array_p ) const 
{ 
    static_assert( cppt::is_iterator<T>::value );
    //static_assert( sizeof(T) == ITEM_SIZE );
    return std::next(f_array_p, m_preSize_u + m_alignedSize_u + m_postSize_u); 
}


template <uintptr_t ITEM_SIZE, uintptr_t ALIGNMENT_SIZE>
bool array_aligner<ITEM_SIZE,ALIGNMENT_SIZE>::align_with_start( const uintptr_t f_arrayStart_u )
{
    uintptr_t l_startShift_u = f_arrayStart_u % ALIGNMENT_SIZE;
    if (l_startShift_u > 0)
        l_startShift_u = ALIGNMENT_SIZE - l_startShift_u;
    
    cpp_assert( l_startShift_u < ALIGNMENT_SIZE, "Calculation error" );

    // check possibility of alignemnt
    {
        // check for out of frame shifts
        const bool l_shiftOutOfFrame_b = (0 != (l_startShift_u % ITEM_SIZE));
        
        if ( l_shiftOutOfFrame_b )
        {
            set_to_unalignable();
            return false;
        }
    }
    
    // bring shift on correct item size and apply shift
    cpp_assert( 0 == (l_startShift_u % ITEM_SIZE), "Calculation error" );
    l_startShift_u = l_startShift_u / ITEM_SIZE;

    cpp_assert( 0 == m_preSize_u, "Invalid array size" );
    cpp_assert( l_startShift_u <= m_alignedSize_u, "Invalid array size" );
    m_preSize_u += l_startShift_u;
    m_alignedSize_u -= l_startShift_u;

    return true;  // true: everything fine
}


template <uintptr_t ITEM_SIZE, uintptr_t ALIGNMENT_SIZE>
void array_aligner<ITEM_SIZE,ALIGNMENT_SIZE>::align_with_end()
{
    static_assert( 0 == (ALIGNMENT_SIZE % ITEM_SIZE) );
    const uintptr_t l_endShift_u = m_alignedSize_u % ( ALIGNMENT_SIZE / ITEM_SIZE );
    cpp_assert( l_endShift_u <= m_alignedSize_u, "Invalid array size" );

    // apply shift
    m_alignedSize_u -= l_endShift_u;
    m_postSize_u += l_endShift_u;
}



} // namespace cppt
