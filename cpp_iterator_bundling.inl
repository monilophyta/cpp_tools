/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "cpp_iterator_bundling.hpp"
#include "cpp_debug.hpp"
#include <algorithm>
#include "cpp_type_traits.hpp"


namespace cppt
{


//--------------------------------------------------------------------------------------------------------------

template <class ITER_T>
inline bool CIteratorBundleValueIterator<ITER_T>::operator==(const this_t& f_other) const
{
    cpp_assert( m_iterPos == f_other.m_iterPos, "Comparing not compariable iterators" );
    return ( m_baseIter == f_other.m_baseIter );
}

template <class ITER_T>
inline bool CIteratorBundleValueIterator<ITER_T>::operator!=(const this_t& f_other) const
{
    cpp_assert( m_iterPos == f_other.m_iterPos, "Comparing not compariable iterators" );
    return ( m_baseIter != f_other.m_baseIter );
}

template <class ITER_T>
inline bool CIteratorBundleValueIterator<ITER_T>::operator> (const this_t& f_other) const
{
    cpp_assert( m_iterPos == f_other.m_iterPos, "Comparing not compariable iterators" );
    return ( m_baseIter >  f_other.m_baseIter );
}

template <class ITER_T>
inline bool CIteratorBundleValueIterator<ITER_T>::operator>=(const this_t& f_other) const
{
    cpp_assert( m_iterPos == f_other.m_iterPos, "Comparing not compariable iterators" );
    return ( m_baseIter >= f_other.m_baseIter );
}

template <class ITER_T>
inline bool CIteratorBundleValueIterator<ITER_T>::operator< (const this_t& f_other) const
{
    cpp_assert( m_iterPos == f_other.m_iterPos, "Comparing not compariable iterators" );
    return ( m_baseIter <  f_other.m_baseIter );
}

template <class ITER_T>
inline bool CIteratorBundleValueIterator<ITER_T>::operator<=(const this_t& f_other) const
{
    cpp_assert( m_iterPos == f_other.m_iterPos, "Comparing not compariable iterators" );
    return ( m_baseIter <= f_other.m_baseIter );
}


/// Differences
template <class ITER_T>
inline auto
CIteratorBundleValueIterator<ITER_T>::operator-( const this_t& f_other ) const -> iter_diff_t
{
    cpp_assert( m_iterPos == f_other.m_iterPos, "Comparing not compariable iterators" );
    return ( m_baseIter - f_other.m_baseIter );
}


//--------------------------------------------------------------------------------------------------------------

template <class ITER_T>
inline CIteratorBundleIterator<ITER_T>
make_iterator_bundle( size_t f_numIters,
                      std::function<ITER_T(size_t)> iter_gen,
                      std::ptrdiff_t l_offset,
                      std::ptrdiff_t l_stepSize )
{
    static_assert( true == is_iterator<ITER_T>::value );

    typedef bundle_traits<ITER_T>                            bundle_traits_t;
    typedef CIteratorBundleIterator<ITER_T>                         bundle_t;

    static_assert( true == std::is_move_constructible<bundle_t>::value );
    static_assert( true == std::is_copy_constructible<bundle_t>::value );
    
    typedef typename bundle_traits_t::bundle_container_t  bundle_container_t;    

    /// create container instance
    bundle_container_t* l_bundle_container_ptr = new bundle_container_t();

    /// fill container with iterators
    l_bundle_container_ptr->reserve( f_numIters );
    for ( size_t i=0; i < f_numIters; ++i )
    {
        l_bundle_container_ptr->emplace_back( iter_gen(i) );
    }
    
    return bundle_t( l_bundle_container_ptr, l_offset, l_stepSize );
}


//--------------------------------------------------------------------------------------------------------------


template <typename ITER_T>
inline auto
CIteratorBundleValue<ITER_T>::value( size_t f_iterIdx ) const -> value_reference_t
{
    cpp_bounds_check( f_iterIdx < size() );
    
    iter_t it = std::next( (*m_bundle_ptr)[ f_iterIdx ], m_iterPos );
    return *it;
}



template <typename ITER_T>
template <typename IDX_ITER_T, typename OITER_T>
inline OITER_T
CIteratorBundleValue<ITER_T>::get_indexed_values(
                  IDX_ITER_T f_idxIter,
            const IDX_ITER_T f_idxEnd,
                     OITER_T f_outIter ) const
{
    typedef typename std::iterator_traits<IDX_ITER_T>::value_type idx_t;
    
    auto kernel = [=] ( idx_t f_iterIdx ) -> const value_reference_t
    {
        cpp_bounds_check( f_iterIdx < size() );
        return this->value( f_iterIdx );
    };

    return std::transform( f_idxIter, f_idxEnd,
                           f_outIter,
                           kernel );
}


//--------------------------------------------------------------------------------------------------------------

template <typename ITER_T>
inline bool CIteratorBundleIterator<ITER_T>::operator==(const this_t& f_other) const
{
    cpp_assert( std::equal( m_bundle_ptr->cbegin(), m_bundle_ptr->cend(), f_other.m_bundle_ptr->cbegin() ), 
                "Comparing not compariable iterators" );
    return ( iter_pos() == f_other.iter_pos() );
}

template <typename ITER_T>
inline bool CIteratorBundleIterator<ITER_T>::operator!=(const this_t& f_other) const
{
    cpp_assert( std::equal( m_bundle_ptr->cbegin(), m_bundle_ptr->cend(), f_other.m_bundle_ptr->cbegin() ), 
                "Comparing not compariable iterators" );
    return ( iter_pos() != f_other.iter_pos() );
}

template <typename ITER_T>
inline bool CIteratorBundleIterator<ITER_T>::operator> (const this_t& f_other) const
{
    cpp_assert( std::equal( m_bundle_ptr->cbegin(), m_bundle_ptr->cend(), f_other.m_bundle_ptr->cbegin() ), 
                "Comparing not compariable iterators" );
    return ( iter_pos() >  f_other.iter_pos() );
}

template <typename ITER_T>
inline bool CIteratorBundleIterator<ITER_T>::operator>=(const this_t& f_other) const
{
     cpp_assert( std::equal( m_bundle_ptr->cbegin(), m_bundle_ptr->cend(), f_other.m_bundle_ptr->cbegin() ), 
                "Comparing not compariable iterators" );
    return ( iter_pos() >= f_other.iter_pos() );
}

template <typename ITER_T>
inline bool CIteratorBundleIterator<ITER_T>::operator< (const this_t& f_other) const
{
    cpp_assert( std::equal( m_bundle_ptr->cbegin(), m_bundle_ptr->cend(), f_other.m_bundle_ptr->cbegin() ), 
                "Comparing not compariable iterators" );
    return ( iter_pos() <  f_other.iter_pos() );
}

template <typename ITER_T>
inline bool CIteratorBundleIterator<ITER_T>::operator<=(const this_t& f_other) const
{
    cpp_assert( std::equal( m_bundle_ptr->cbegin(), m_bundle_ptr->cend(), f_other.m_bundle_ptr->cbegin() ), 
                "Comparing not compariable iterators" );
    return ( iter_pos() <= f_other.iter_pos() );
}


template <typename ITER_T>
inline auto CIteratorBundleIterator<ITER_T>::operator-( const this_t& f_other ) const -> iter_diff_t
{
    cpp_assert( std::equal( m_bundle_ptr->cbegin(), m_bundle_ptr->cend(), f_other.m_bundle_ptr->cbegin() ), 
                "Comparing not compariable iterators" );
    return ( m_iterPos - f_other.m_iterPos );
}


}  // namespace cppt


