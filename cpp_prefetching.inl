/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "cpp_prefetching.hpp"



namespace cppt
{



template <typename T>
template <typename ET>
constexpr inline
void PreFetcher<T>::single_prefetch( ET* f_ptr, ETemporalLocality f_tloc_e )
{
    static_assert( sizeof(ET) == sizeof(T) );
 
//#ifndef __clang__
#if 0
    if (std::is_const<ET>::value)
    {
        __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_ONLY), static_cast<int>(f_tloc_e) );
    }
    else
    {
        __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_WRITE), static_cast<int>(f_tloc_e) );
    }
#else  // #ifndef(__clang__)
    // due to clang error: must be a constant integer
    if (std::is_const<ET>::value)
    {
        switch (f_tloc_e)
        {
            case ETemporalLocality::NONE:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_ONLY), static_cast<int>(ETemporalLocality::NONE) );
                break;
            case ETemporalLocality::LOW:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_ONLY), static_cast<int>(ETemporalLocality::LOW) );
                break;
            case ETemporalLocality::MIDDLE:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_ONLY), static_cast<int>(ETemporalLocality::MIDDLE) );
                break;
            case ETemporalLocality::HIGH:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_ONLY), static_cast<int>(ETemporalLocality::HIGH) );
                break;
            default:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_ONLY) );
        }
    }
    else
    {
        switch (f_tloc_e)
        {
            case ETemporalLocality::NONE:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_WRITE), static_cast<int>(ETemporalLocality::NONE) );
                break;
            case ETemporalLocality::LOW:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_WRITE), static_cast<int>(ETemporalLocality::LOW) );
                break;
            case ETemporalLocality::MIDDLE:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_WRITE), static_cast<int>(ETemporalLocality::MIDDLE) );
                break;
            case ETemporalLocality::HIGH:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_WRITE), static_cast<int>(ETemporalLocality::HIGH) );
                break;
            default:
                __builtin_prefetch( f_ptr, static_cast<int>(EPreFMode::READ_WRITE) );
        }
    }
#endif  // #ifndef(__clang__)
}

template <typename T>
template <typename ET>
constexpr inline
void PreFetcher<T>::dispatch( size_t f_offset_u, ET* f_ptr )
{
    single_prefetch( f_ptr + f_offset_u );
}


template <typename T>
template <typename ET>
constexpr inline
void PreFetcher<T>::dispatch( size_t f_offset_u, ET* f_ptr, ETemporalLocality f_tloc_e )
{
    single_prefetch( f_ptr + f_offset_u, f_tloc_e );
}


template <typename T>
template <typename ET, typename... Args>
constexpr inline
void PreFetcher<T>::dispatch( size_t f_offset_u, ET* f_ptr, Args... args )
{
    single_prefetch( f_ptr + f_offset_u );
    dispatch( f_offset_u, args... );
}


template <typename T>
template <typename ET, typename... Args>
constexpr inline
void PreFetcher<T>::dispatch( size_t f_offset_u, ET* f_ptr, ETemporalLocality f_tloc_e, Args... args )
{
    single_prefetch( f_ptr + f_offset_u, f_tloc_e );
    dispatch( f_offset_u, args... );
}


template <typename T>
template <typename... Args>
constexpr inline
void PreFetcher<T>::multiple_prefetch( size_t f_offset_u, Args... args )
{
    dispatch( f_offset_u, args... );
}


} // namespace cppt

