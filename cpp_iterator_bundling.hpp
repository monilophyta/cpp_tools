/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <iterator>
#include <functional>
#include <memory>
#include "cpp_type_traits.hpp"



namespace cppt
{

//--------------------------------------------------------------------------------------------------------------

template <typename ITER_T>
struct CIteratorBundleIterator;

template <typename ITER_T>
struct CIteratorBundleValue;

template <typename ITER_T>
struct CIteratorBundleValueIterator;

//--------------------------------------------------------------------------------------------------------------

template <class ITER_T>
inline CIteratorBundleIterator<ITER_T> make_iterator_bundle( size_t f_numIters, 
                                                             std::function<ITER_T(size_t)> iter_gen,
                                                             std::ptrdiff_t l_offset=0,
                                                             std::ptrdiff_t l_stepSize=1 );


//--------------------------------------------------------------------------------------------------------------


template <typename ITER_T>
struct bundle_traits
{
    typedef ITER_T                                                  iter_t;
    typedef std::iterator_traits<iter_t>                     iter_traits_t;


    static_assert(   std::is_same<typename iter_traits_t::iterator_category, std::bidirectional_iterator_tag>::value
                  || std::is_same<typename iter_traits_t::iterator_category, std::random_access_iterator_tag>::value );

    typedef typename iter_traits_t::value_type                     value_t;
    typedef typename iter_traits_t::reference            value_reference_t;
    typedef typename iter_traits_t::difference_type            iter_diff_t;

    typedef std::vector<iter_t>                         bundle_container_t;
    typedef std::shared_ptr<const bundle_container_t>         bundle_ptr_t;

    typedef CIteratorBundleIterator<iter_t>                           type;
};


//--------------------------------------------------------------------------------------------------------------

template <typename ITER_T>
struct CIteratorBundleValueIterator
        : public std::iterator< typename bundle_traits<ITER_T>::bundle_container_t::iterator::iterator_category,
                                typename bundle_traits<ITER_T>::value_t,
                                typename bundle_traits<ITER_T>::bundle_container_t::iterator::difference_type,
                                ITER_T,
                                typename bundle_traits<ITER_T>::value_reference_t> 
{

private:

    typedef ITER_T                                                             iter_t;
    
    typedef CIteratorBundleValueIterator<iter_t>                               this_t;
    typedef bundle_traits<iter_t>                                     bundle_traits_t;
    
    typedef typename bundle_traits_t::bundle_container_t::const_iterator  base_iter_t;
    typedef typename bundle_traits_t::iter_diff_t                         iter_diff_t;

public:

    typedef typename std::iterator_traits<this_t>::value_type                 value_t;
    typedef typename std::iterator_traits<this_t>::reference              reference_t;
    typedef typename std::iterator_traits<this_t>::pointer                  pointer_t;

    friend struct CIteratorBundleValue<iter_t>;
    friend struct CIteratorBundleIterator<iter_t>;

private:

    base_iter_t       m_baseIter;
    iter_diff_t       m_iterPos;


    inline CIteratorBundleValueIterator( base_iter_t&& f_other, iter_diff_t f_iterPos )
        : m_baseIter( std::move( f_other ) )
        , m_iterPos( f_iterPos )
    {}

public:

    
    inline CIteratorBundleValueIterator()
        : m_baseIter()
        , m_iterPos( std::numeric_limits<iter_diff_t>::max() )
    {}


    inline iter_diff_t iter_pos() const { return m_iterPos; }

    /// Iterator comparison
    inline bool operator==(const this_t& f_other) const;
    inline bool operator!=(const this_t& f_other) const;
    inline bool operator> (const this_t& f_other) const;
    inline bool operator>=(const this_t& f_other) const;
    inline bool operator< (const this_t& f_other) const;
    inline bool operator<=(const this_t& f_other) const;

    /// Base forward iterator progress
    inline this_t& operator++()      {                     ++m_baseIter; return (*this); }
    inline this_t& operator++(int)   { this_t aux = *this; ++m_baseIter; return (  aux); }

    /// Base backward iterator progress
    inline this_t& operator--()      {                     --m_baseIter; return (*this); }
    inline this_t& operator--(int)   { this_t aux = *this; --m_baseIter; return (  aux); }


    /// Base backward iterator progress
    inline this_t operator+( iter_diff_t n ) const { return this_t( std::move( std::next( m_baseIter, n ), m_iterPos ) ); }
    inline this_t operator-( iter_diff_t n ) const { return this_t( std::move( std::prev( m_baseIter, n ), m_iterPos ) ); }

    inline this_t& operator+=( iter_diff_t n ) { std::advance( m_baseIter,  n ); return (*this); }
    inline this_t& operator-=( iter_diff_t n ) { std::advance( m_baseIter, -n ); return (*this); }


    /// Differences
    inline iter_diff_t operator-( const this_t& f_other ) const;

    // Dereferencing
    inline reference_t operator*() const { return *std::next( *m_baseIter, m_iterPos ); }
    inline pointer_t operator->() const { return std::next( *m_baseIter, m_iterPos ); }

    inline reference_t operator[]( iter_diff_t n ) const { return *std::next( *std::next( m_baseIter, n ), m_iterPos ); }
};


//--------------------------------------------------------------------------------------------------------------


template <typename ITER_T>
struct CIteratorBundleValue
{
private:

    typedef ITER_T                                                    iter_t;
    typedef CIteratorBundleValue<iter_t>                              this_t;
    typedef bundle_traits<iter_t>                            bundle_traits_t;
    typedef typename bundle_traits_t::bundle_ptr_t              bundle_ptr_t;
    typedef typename bundle_traits_t::iter_diff_t                iter_diff_t;
    //typedef typename bundle_traits_t::value_t                        value_t;

public:

    friend struct CIteratorBundleIterator<iter_t>;

    typedef CIteratorBundleValueIterator<iter_t>                  iterator_t;
    typedef typename bundle_traits_t::value_reference_t    value_reference_t;


private:

    bundle_ptr_t      m_bundle_ptr;
    iter_diff_t       m_iterPos;


    inline CIteratorBundleValue( const bundle_ptr_t& f_bundle_ptr, iter_diff_t f_iterPos )
        : m_bundle_ptr( f_bundle_ptr )
        , m_iterPos( f_iterPos )
    {}


public:

    inline CIteratorBundleValue()
        : m_bundle_ptr(nullptr)
        , m_iterPos( std::numeric_limits<iter_diff_t>::max() )
    {}


    inline size_t size() const { return ( (true == bool(m_bundle_ptr)) ? m_bundle_ptr->size() : 0); }

    /// Dereferencing the iterators within the bundle
    inline value_reference_t value( size_t f_iterIdx ) const;
    inline value_reference_t operator[]( size_t f_iterIdx ) const { return value( f_iterIdx ); }

    inline iterator_t begin() const { return iterator_t( std::move( m_bundle_ptr->cbegin() ), m_iterPos ); }
    inline iterator_t end() const { return iterator_t( std::move( m_bundle_ptr->cend() ), m_iterPos ); }


    template <typename IDX_ITER_T, typename OITER_T>
    inline OITER_T get_indexed_values( IDX_ITER_T f_idxIter, const IDX_ITER_T f_idxEnd,
                                       OITER_T f_outIter ) const;
    

    /*inline       this_t& operator*()       { return *this; }
    inline const this_t& operator*() const { return *this; }*/

    inline       this_t* operator->()       { return this; }
    inline const this_t* operator->() const { return this; }
    
    /*inline       this_t operator&()       { return *this; }
    inline const this_t operator&() const { return *this; }*/

    /// for debugging and testing
    inline iter_diff_t iter_pos() const { return m_iterPos; }
};



//--------------------------------------------------------------------------------------------------------------


template <typename ITER_T>
struct CIteratorBundleIterator
        : public std::iterator< std::random_access_iterator_tag,
                                CIteratorBundleValue<ITER_T>,
                                typename std::iterator_traits< ITER_T >::difference_type,
                                CIteratorBundleValue<ITER_T>,   // Pointer
                                CIteratorBundleValue<ITER_T> >  // Reference
{
private:

    typedef ITER_T                                                    iter_t;
    typedef bundle_traits<iter_t>                            bundle_traits_t;
    typedef CIteratorBundleIterator<iter_t>                           this_t;
    typedef CIteratorBundleValueIterator<iter_t>           column_iterator_t;
    
    typedef typename bundle_traits_t::bundle_ptr_t              bundle_ptr_t;
    typedef typename bundle_traits_t::iter_diff_t                iter_diff_t;
    typedef typename bundle_traits_t::bundle_container_t  bundle_container_t;

    typedef CIteratorBundleValue<iter_t>                      bundle_value_t;
    static_assert( true == std::is_move_constructible<bundle_value_t>::value );


    bundle_ptr_t                 m_bundle_ptr;
    iter_diff_t                  m_iterPos;
    iter_diff_t                  m_stepSize;


    inline CIteratorBundleIterator( const bundle_container_t* f_bundleContainer, 
                                                 iter_diff_t  f_initPos,
                                                 iter_diff_t  f_stepSize )
        : m_bundle_ptr( f_bundleContainer )
        , m_iterPos( f_initPos )
        , m_stepSize( f_stepSize )
    {}


public:


    friend this_t make_iterator_bundle<iter_t>( size_t f_numIters, 
                                                std::function<iter_t (size_t)> iter_gen,
                                                std::ptrdiff_t l_offset,
                                                std::ptrdiff_t l_stepSize );


    inline CIteratorBundleIterator()
        : m_bundle_ptr(nullptr)
        , m_iterPos( std::numeric_limits<iter_diff_t>::max() )
        , m_stepSize(0)
    {}

    inline iter_diff_t iter_pos() const { return (m_iterPos * m_stepSize); }

    /// Iterator comparison
    inline bool operator==(const this_t& f_other) const;
    inline bool operator!=(const this_t& f_other) const;
    inline bool operator> (const this_t& f_other) const;
    inline bool operator>=(const this_t& f_other) const;
    inline bool operator< (const this_t& f_other) const;
    inline bool operator<=(const this_t& f_other) const;

    /// Base forward iterator progress
    inline this_t& operator++()      {                     ++m_iterPos; return (*this); }
    inline this_t& operator++(int)   { this_t aux = *this; ++m_iterPos; return (  aux); }

    /// Base backward iterator progress
    inline this_t& operator--()      {                     --m_iterPos; return (*this); }
    inline this_t& operator--(int)   { this_t aux = *this; --m_iterPos; return (  aux); }


    /// Base backward iterator progress
    inline this_t operator+( iter_diff_t n ) const { this_t aux = *this; aux.m_iterPos += n; return aux; }
    inline this_t operator-( iter_diff_t n ) const { this_t aux = *this; aux.m_iterPos -= n; return aux; }

    inline this_t& operator+=( iter_diff_t n ) { m_iterPos += n; return (*this); }
    inline this_t& operator-=( iter_diff_t n ) { m_iterPos -= n; return (*this); }


    /// Differences
    inline iter_diff_t operator-( const this_t& f_other ) const;


    /// Iterator dereferencing 
    //inline bundle_value_t operator*() const { return bundle_value_t( m_bundle_ptr, iter_pos() ); }
    inline column_iterator_t operator*() const { return column_iterator_t( std::move( m_bundle_ptr->cbegin() ), iter_pos() ); }

    inline bundle_value_t operator->() const { return bundle_value_t( m_bundle_ptr, iter_pos() ); }

    //inline bundle_value_t operator[]( std::size_t n ) const {
    //    return bundle_value_t( m_bundle_ptr, (m_iterPos + static_cast<iter_diff_t>(n) ) * m_stepSize ); }
    
    inline column_iterator_t operator[]( std::size_t n ) const {
        return column_iterator_t( std::move( m_bundle_ptr->cbegin() ),
                                  (m_iterPos + static_cast<iter_diff_t>(n) ) * m_stepSize ); }
};


} // namespace cppt

