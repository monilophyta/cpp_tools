/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <type_traits>


namespace cppt
{

#define POWER_OF_TWO_PADDING( SIZE )  int8_t  ___padding[ cppt::nextPowerOfTwo( SIZE ) - (SIZE) ]



template <typename T>
inline constexpr
typename std::enable_if< std::is_integral<T>::value, bool>::type isPowerOfTwo( T n );



/*!
    \brief calculates the next larger power of twp

    \param n integer for which the next larger power of two is calculated

    \return for n==0 zero is returned, otherwise the next larger power of two
*/
template <typename T>
inline constexpr 
typename std::enable_if< std::is_integral<T>::value, T>::type nextPowerOfTwo( T n );



}  // namespace cppt

