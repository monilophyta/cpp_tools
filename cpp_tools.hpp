/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "cpp_target_os.hpp"
#include "cpp_memory_caching.hpp"

#include "cpp_debug.hpp"
#include "cpp_type_traits.hpp"
#include "cpp_postulate.hpp"

#include "cpp_madvice.hpp"

#include "cpp_prefetching.hpp"
#include "cpp_prefetching.inl"

#include "struct_padding.hpp"
#include "struct_padding.inl"

#include "cpp_memory_alignment.hpp"
#include "cpp_memory_alignment.inl"

#include "todo.hpp"

#include "cpp_iterator_bundling.hpp"
#include "cpp_iterator_bundling.inl"
#include "cpp_iterator_utils.hpp"
