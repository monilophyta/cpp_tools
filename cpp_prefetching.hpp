/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstddef>
#include "cpp_target_os.hpp"


/** Inspirations
 * # Prefetching:
 *   https://www.cs.cmu.edu/afs/cs/academic/class/15745-s16/www/lectures/L21-Prefetching-Arrays.pdf
 *
 * # Variadic Templates
 *   https://www.modernescpp.com/index.php/constexpr-variables-and-objects
 */

namespace cppt
{


constexpr std::size_t DEFAULT_CACHE_LINES_AHEAD = 64;


enum class EPreFMode : int
{ 
    READ_ONLY = 0,
    READ_WRITE = 1
};

enum class ETemporalLocality : int
{
    NONE   = 0,
    LOW    = 1,
    MIDDLE = 2,
    HIGH   = 3
};


template <typename T>
struct PreFetcher
{
public:

    static_assert( (CACHE_LINE_SIZE % sizeof(T)) == 0 );

    static constexpr std::size_t num_items_per_cache_line = CACHE_LINE_SIZE / sizeof(T);
    static_assert( num_items_per_cache_line > 0 );

    std::size_t m_numItemsAhead_u;


    constexpr inline 
    PreFetcher( std::size_t f_cacheLinesAhead_u = DEFAULT_CACHE_LINES_AHEAD )
        : m_numItemsAhead_u( f_cacheLinesAhead_u * num_items_per_cache_line )
    {}


    template <typename... Args>
    constexpr inline
    void prolog( Args... args ) const
    {
        for (size_t f_offset_u = 0; f_offset_u < m_numItemsAhead_u; f_offset_u += num_items_per_cache_line )
        {
            multiple_prefetch( f_offset_u, args... );
        }
    }

    template <typename... Args>
    constexpr inline
    void prefetch( Args... args ) const
    {
        multiple_prefetch( m_numItemsAhead_u, args... );
    }


private:

    template <typename ET>
    static constexpr inline
    void single_prefetch( ET* f_ptr, ETemporalLocality f_tloc_e = ETemporalLocality::HIGH );

    template <typename ET>
    static  constexpr inline
    void dispatch( size_t f_offset_u, ET* f_ptr );

    template <typename ET>
    static constexpr inline
    void dispatch( size_t f_offset_u, ET* f_ptr, ETemporalLocality f_tloc_e );

    template <typename ET, typename... Args>
    static constexpr inline
    void dispatch( size_t f_offset_u, ET* f_ptr, Args... args );

    template <typename ET, typename... Args>
    static constexpr inline
    void dispatch( size_t f_offset_u, ET* f_ptr, ETemporalLocality f_tloc_e, Args... args );

    template <typename... Args>
    static constexpr inline
    void multiple_prefetch( size_t f_offset_u, Args... args );
};


} // namespace cppt

