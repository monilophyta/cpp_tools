/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <limits>
#include <cmath>
#include <functional>
#include <cassert>

// For more details, check: https://gcc.gnu.org/wiki/FloatingPointMath

// checking detection of NaN and inf
namespace cppt_floating_checks
{

template <typename FT>
struct FloatingChecks
{

    inline FloatingChecks()
    {
        // checking detection of NaN and inf
        assert( true == std::isnan( std::numeric_limits<FT>::signaling_NaN() ) );
        assert( true == std::isnan( std::numeric_limits<FT>::quiet_NaN() ) );
        assert( true == std::isinf( std::numeric_limits<FT>::infinity() ) );
       
       assert( false == std::isfinite( std::numeric_limits<FT>::signaling_NaN() ) );
       assert( false == std::isfinite( std::numeric_limits<FT>::quiet_NaN() ) );
       assert( false == std::isfinite( std::numeric_limits<FT>::infinity() ) );
    }

    // verify comparsions with NaN and inf
    static_assert( true == (std::numeric_limits<FT>::signaling_NaN() != std::numeric_limits<FT>::signaling_NaN()) );
    static_assert( false == (std::numeric_limits<FT>::signaling_NaN() == std::numeric_limits<FT>::signaling_NaN()) );
    static_assert( false == (0 < std::numeric_limits<FT>::signaling_NaN()) );
    static_assert( false == (0 > std::numeric_limits<FT>::signaling_NaN()) );

    static_assert( true == (std::numeric_limits<FT>::quiet_NaN() != std::numeric_limits<FT>::quiet_NaN()) );
    static_assert( false == (std::numeric_limits<FT>::quiet_NaN() == std::numeric_limits<FT>::quiet_NaN()) );
    static_assert( false == (0 < std::numeric_limits<FT>::quiet_NaN()) );
    static_assert( false == (0 > std::numeric_limits<FT>::quiet_NaN()) );

    static_assert( true == (std::numeric_limits<FT>::quiet_NaN() != std::numeric_limits<FT>::signaling_NaN()) );
    static_assert( false == (std::numeric_limits<FT>::quiet_NaN() == std::numeric_limits<FT>::signaling_NaN()) );

    static_assert( true == (std::numeric_limits<FT>::quiet_NaN() != std::numeric_limits<FT>::infinity()) );
    static_assert( false == (std::numeric_limits<FT>::quiet_NaN() == std::numeric_limits<FT>::infinity()) );

    // this is unintuitiv!
    static_assert( false == (std::numeric_limits<FT>::infinity() != std::numeric_limits<FT>::infinity()) );
    static_assert( true == (std::numeric_limits<FT>::infinity() == std::numeric_limits<FT>::infinity()) );

    static_assert( true == (0 < std::numeric_limits<FT>::infinity()) );
    static_assert( true == (0 > -std::numeric_limits<FT>::infinity()) );
};

// explicit instantiation of check
template struct FloatingChecks<float>;
template struct FloatingChecks<double>;
template struct FloatingChecks<long double>;

FloatingChecks<float> x();
FloatingChecks<double> y();
FloatingChecks<long double> z();


} // namespace cppt_floating_checks



namespace cppt
{


} // namespace cppt

