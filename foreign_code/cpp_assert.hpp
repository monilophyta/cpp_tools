#pragma once


#include <exception>
#include <string>
#include <sstream>
#include <iostream>
#include <limits>



/*  Assertion exceptions

    Based on https://www.softwariness.com/articles/assertions-in-cpp/
    by the user "David"
*/




#define davids_smart_assert(EXPRESSION, MESSAGE) \
{ \
    if(!(EXPRESSION)) \
    { \
        throw asc::CAssertionFailureException(#EXPRESSION, __FILE__, __LINE__, (asc::CStreamFormatter() << MESSAGE)); \
    } \
} 




namespace asc 
{


/// Helper class for formatting assertion message
class CStreamFormatter
{
private:

    std::ostringstream m_stream;

public:

    inline CStreamFormatter() : m_stream()
    {}


    operator std::string() const
    {
        return m_stream.str();
    }

    template<typename T>
    CStreamFormatter& operator << (const T& value)
    {
        m_stream << value;
        return *this;
    }
};


/// Exception type for assertion failures
class CAssertionFailureException : public std::exception
{
private:

    const char* m_expression_ac;
    const char* m_file_ac;
    int32_t     m_line_i32;
    std::string m_message_s;
    std::string m_report_s;
 
public:
 
    inline CAssertionFailureException();

    /// Construct an assertion failure exception
    inline CAssertionFailureException(const char* f_expression, const char* f_file, int32_t f_line, const std::string& f_message);

    inline ~CAssertionFailureException();

    inline CAssertionFailureException( const CAssertionFailureException& other );
    inline CAssertionFailureException& operator=( const CAssertionFailureException& other );

    /// Log error before throwing
    void LogError()
    {
        std::cerr << m_report_s << std::endl;
    }

 
    /// The assertion message
    virtual const char* what() const throw()
    {
        return m_report_s.c_str();
    }
 
    /// The expression which was asserted to be true
    const char* Expression() const
    {
        return m_expression_ac;
    }
 
    /// Source file
    const char* File() const
    {
        return m_file_ac;
    }
 
    /// Source line
    int32_t Line() const
    {
        return m_line_i32;
    }
 
    /// Description of failure
    const char* Message() const
    {
        return m_message_s.c_str();
    }
};



inline 
CAssertionFailureException::CAssertionFailureException()
    : m_expression_ac( nullptr )
    , m_file_ac( nullptr )
    , m_line_i32(-std::numeric_limits<int32_t>::max())
    , m_message_s()
    , m_report_s()
{}


/// Construct an assertion failure exception
inline
CAssertionFailureException::CAssertionFailureException(const char* f_expression, const char* f_file, int32_t f_line, const std::string& f_message)
    : m_expression_ac( f_expression )
    , m_file_ac( f_file )
    , m_line_i32( f_line )
    , m_message_s( f_message )
    , m_report_s()
{
    std::ostringstream outputStream;

    if (!m_message_s.empty())
    {
        outputStream << m_message_s << ": ";
    }

    std::string expressionString = m_expression_ac;

    if (expressionString == "false" || expressionString == "0" || expressionString == "FALSE")
    {
        outputStream << "Unreachable code assertion";
    }
    else
    {
        outputStream << "Assertion '" << m_expression_ac << "'";
    }

    outputStream << " failed in file '" << m_file_ac << "' line " << m_line_i32;
    m_report_s = outputStream.str();

    LogError();
}


inline
CAssertionFailureException::~CAssertionFailureException()
{
    m_expression_ac = nullptr;
    m_file_ac       = nullptr;
    m_line_i32      = std::numeric_limits<int32_t>::max();
}


inline
CAssertionFailureException::CAssertionFailureException( const CAssertionFailureException& other )
    : m_expression_ac( other.m_expression_ac )
    , m_file_ac( other.m_file_ac )
    , m_line_i32(-other.m_line_i32 )
    , m_message_s(other.m_message_s)
    , m_report_s(other.m_report_s)
{}


inline
CAssertionFailureException& CAssertionFailureException::operator=( const CAssertionFailureException& other )
{
    m_expression_ac = other.m_expression_ac;
    m_file_ac       = other.m_file_ac;
    m_line_i32      = other.m_line_i32;

    m_message_s     = other.m_message_s;
    m_report_s      = other.m_report_s;

    return *this;
}


} // namespace asc
