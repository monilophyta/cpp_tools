/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


// -----------------------------------------------------------------------


#if defined(__linux__) || defined(__linux) // || defined(linux) 
    
    #define TARGET_UNIX
    #define TARGET_LINUX

#elif defined(__unix__) || defined(__unix) || (defined (__APPLE__) && defined (__MACH__))
    
    #define TARGET_UNIX

#elif defined(_WIN32) || defined(WIN32) || defined (__CYGWIN__)
    
    #define TARGET_WINDOWS

#else
    static_assert( false, "operating system not detected");
#endif


// -----------------------------------------------------------------------


#if defined(__ARM_ARCH)

    #define ARM_ARCH __ARM_ARCH

#elif defined(__x86_64__)

    #define X86_ARCH __x86_64__
    #define X86_64_ARCH __x86_64__

#elif defined(__i386__)

    #define X86_ARCH __i386__
    #define X86_32_ARCH __i386__
#else
    static_assert( false, "architecture not detected");
#endif



