/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cassert>
#include "foreign_code/cpp_assert.hpp"


#ifndef NDEBUG

    /// cppp_assert is indicating a prooramming error
    #define cpp_assert(EXPRESSION, MESSAGE) davids_smart_assert(EXPRESSION, MESSAGE)
#else // NDEBUG
    #define cpp_assert(EXPRESSION, MESSAGE)
#endif // NDEBUG


#define cpp_bounds_check(EXPRESSION) cpp_assert( EXPRESSION, "Out of bounds access" )

