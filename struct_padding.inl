/*
 * Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
 *
 * Licensed under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version..
 * See LICENSE file in the project root for full license information.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "struct_padding.hpp"


namespace cppt
{


namespace hidden
{

template <typename T>
inline constexpr bool __isPowerOfTwo( T n )
{
    return ( ( T(0) == (n % T(2)) ) 
             ? __isPowerOfTwo( n / T(2) )
             : ( n == T(1) ) );
}


template <typename T>
inline constexpr T __nextPowerOfTwo( T n )
{
    return ( ( n > T(1) )
             ? ( T(2) * __nextPowerOfTwo( ( n + T(1) ) / T(2) ) )
             : T(1) );
}

}  // namespace hidden



//----------------------------------------------------------------------------------


template <typename T>
inline constexpr
typename std::enable_if< std::is_integral<T>::value, bool>::type
isPowerOfTwo( T n )
{
    return ( ( T(0) == n ) 
             ? true
             : hidden::__isPowerOfTwo( n ) );
}


/*!
    \brief calculates the next larger power of twp

    \param n integer for which the next larger power of two is calculated

    \return for n==0 zero is returned, otherwise the next larger power of two
*/
template <typename T>
inline constexpr 
typename std::enable_if< std::is_integral<T>::value, T>::type
nextPowerOfTwo( T n )
{
    return ( ( T(0) >= n ) ? T(0) : hidden::__nextPowerOfTwo( n ) );
}

static_assert( nextPowerOfTwo( 0 ) == 0 );
static_assert( nextPowerOfTwo( 1 ) == 1 );
static_assert( nextPowerOfTwo( 2 ) == 2 );
static_assert( nextPowerOfTwo( 3 ) == 4 );
static_assert( nextPowerOfTwo( 4 ) == 4 );
static_assert( nextPowerOfTwo( 30 ) == 32 );
static_assert( nextPowerOfTwo( 33 ) == 64 );



}  // namespace cppt
